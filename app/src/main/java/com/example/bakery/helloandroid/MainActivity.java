package com.example.bakery.helloandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView tvHello;
    private EditText etText;
    private EditText etNum;
    private EditText et1;
    private EditText et2;
    private TextView tvResult;
    private Button btnCopy;
    private Button btnSum;
    private RadioGroup rgRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvHello = findViewById(R.id.tvHello);
        etText = findViewById(R.id.etText);
        etNum = findViewById(R.id.etNum);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        tvResult = findViewById(R.id.tvResult);
        btnCopy = findViewById(R.id.btnCopy);
        btnSum = findViewById(R.id.btnSum);
        rgRadio = findViewById(R.id.rgRadio);
        initInstances();


    }

    private void initInstances() {
        etNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    tvHello.setText(etText.getText());
                    return true;
                }
                return false;
            }
        });

        btnCopy.setOnClickListener(listener);
        btnSum.setOnClickListener(listener);

    }

    private void calculate() {
        int num1 = 0;
        int num2 = 0;
        int sum = 0;
        try {
            num1 = Integer.parseInt(et1.getText().toString().replace(".", ""));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Null:num1", Toast.LENGTH_SHORT).show();
        }
        try {
            num2 = Integer.parseInt(et2.getText().toString().replace(".", ""));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Null:num2", Toast.LENGTH_SHORT).show();

        }

        switch (rgRadio.getCheckedRadioButtonId()) {
            case R.id.rbPlus:
                sum = num1 + num2;
                break;
            case R.id.rbMinus:
                sum = num1 - num2;
                break;
            case R.id.rbMultiply:
                sum = num1 * num2;
                break;
            case R.id.rbDivine:
                sum = num1 / num2;
                break;

        }
        tvResult.setText("= " + sum);

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnCopy) {
                tvHello.setText(etText.getText());
            } else if (v == btnSum) {
                calculate();
            }
        }
    };


}
